using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class Movement : MonoBehaviour
{
   [SerializeField] private float PlayerSpeed = 5f;
   [SerializeField] private float gravityValue = -9.81f;
   [SerializeField] private float controllerDeadzone = 0.1f;

   private CharacterController _controller;
   private Vector2 movement;
   private Vector2 aim;

   private Vector3 playerVelocity;

   private PlayerController PlayerController;
   private PlayerInput _playerInput;
   ///
    
  


   private void Awake()
   {
      _controller = GetComponent<CharacterController>();
      PlayerController = new PlayerController();
      _playerInput = GetComponent<PlayerInput>();
   }

   private void OnEnable()
   {
      PlayerController.Enable();
   }

   private void OnDisable()
   {
      PlayerController.Disable();
   }

   private void Update()
   {
      Pause();
      HandleInput();
      HandleMovement();
      HandleRotation();
      

   }

   void Pause()
   {
      if ( Time.timeScale == 0f)
      {
         OnDisable();
      }
      if (Time.timeScale != 0f)
      {
         OnEnable();
      }
   }
   void HandleInput()
   {
      movement = PlayerController.Control.Movement.ReadValue<Vector2>();
      aim = PlayerController.Control.Aim.ReadValue<Vector2>();
   }

   void HandleMovement()
   {
      Vector3 move = new Vector3(movement.x, 0, movement.y);
      _controller.Move(move * Time.deltaTime * PlayerSpeed);

      playerVelocity.y += gravityValue * Time.deltaTime;
      _controller.Move(playerVelocity * Time.deltaTime);
   }

   void HandleRotation()
   {
      Ray ray = Camera.main.ScreenPointToRay(aim);
      Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
      float raydistance;

      if (groundPlane.Raycast(ray,out raydistance))
      {
         Vector3 point = ray.GetPoint(raydistance);
         LookAt(point);
      }

   }

   private void LookAt(Vector3 lookpoint)
   {
      Vector3 heightCorrectedPoint = new Vector3(lookpoint.x, transform.position.y, lookpoint.z);
      transform.LookAt(heightCorrectedPoint);
   }
   
  /////////
  
  
 
  private void OnCollisionEnter(Collision collision)
  {
    
     if (collision.gameObject.CompareTag("AIDOG"))
     {
        
        Transform Pickup = gameObject.transform.Find("Pickup");

        if (Pickup != null)
        {
           foreach (Transform child in Pickup)
           {
              child.gameObject.transform.SetParent(null);
              child.gameObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);



           }
        }

        
        
         
            
         
     }
  }
  /*
   * 
   */

   
}
