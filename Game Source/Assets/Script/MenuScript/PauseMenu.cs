using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject Optionmenu;
    private bool OptionOn;
    private bool Optionoof = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && Optionoof == false)
        {
            Optionmenu.SetActive(!Optionmenu.gameObject.activeSelf);
        }

        if (Optionmenu.active == true)
        {
            OptionOn = true;
        }
        if (Optionmenu.active == false)
        {
            OptionOn = false;
        }
        optionmunu();
    }

    private void optionmunu()
    {
        if (OptionOn == true)
        {
            Time.timeScale = 0f;
        }
        else if(OptionOn == false)
        {
            Time.timeScale = 1f;
        }
    }
    
    public void resume()
    {
        Optionmenu.SetActive(false);
        Time.timeScale = 1;
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
        Time.timeScale = 1;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    public void Nextlevel(int levelID)
    {
        Time.timeScale = 1;
        string levelname = "Level " + levelID;
        SceneManager.LoadScene(levelname);
    }
}
