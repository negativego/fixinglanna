using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour
{
    public Button[] buttons;
    public GameObject levelButtons;

    private void Awake()
    {
        int UnlockedLevel = PlayerPrefs.GetInt("UnlockedLevel", 1);
        for (int i = 0; i < buttons.Length; i++)
       {
           buttons[i].interactable = false;
       }
       for (int i = 0; i < UnlockedLevel; i++)
      {
          buttons[i].interactable = true;
       }
   }

    public void Openlevel(int levelID)
    {
        string levelname = "Level " + levelID;
        SceneManager.LoadScene(levelname);
    }
 
}
