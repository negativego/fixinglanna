using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;


public class Interactor : MonoBehaviour
{
    public Transform InteractorSource;
    public float InteractRange;
    private Animator animator;

    private void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    private void Update()
    {
       
        if (Input.GetKeyDown(KeyCode.E ))
        {
            Ray r = new Ray(InteractorSource.position, InteractorSource.forward);
            if (Physics.Raycast(r, out RaycastHit hitInfo, InteractRange))
            {
                    
                if (hitInfo.collider.gameObject.TryGetComponent(out IInteractable interactObj))
                {
                    interactObj.Interact(gameObject);
                    animator.SetBool("Hammer",true);
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            animator.SetBool("Hammer",false);
        }
           
        if (Input.GetMouseButtonDown(0))
        {
            Ray r = new Ray(InteractorSource.position, InteractorSource.forward);
            if (Physics.Raycast(r, out RaycastHit hitInfo, InteractRange))
            {
                    
                if (hitInfo.collider.gameObject.TryGetComponent(out IInteractable interactObj))
                {
                    interactObj.Interact(gameObject);
                    animator.SetBool("Hammer",true);
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            animator.SetBool("Hammer",false);
        }
            
        
        
        

        
    }
}