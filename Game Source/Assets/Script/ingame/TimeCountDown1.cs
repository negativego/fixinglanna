using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;
using UnityEngine.Timeline;
using UnityEngine.EventSystems;

public class TimeCountDown1 : MonoBehaviour
{
   [SerializeField] GameObject panel;
   [SerializeField] Image UIFill;
   [SerializeField] TMP_Text UiText;

   public int Duration;
   private int remainingDuration;
   private bool Pause;
   private void Start()
   {
      panel.SetActive(false);
      Being(Duration);
      Time.timeScale = 1;
   }

   private void Being(int Second)
   {
      remainingDuration = Second;
      StartCoroutine(UpdateTimer());
   }

   private IEnumerator UpdateTimer()
   {
      while (remainingDuration >=0)
      {
         if (!Pause)
         {
            UiText.text = $"{remainingDuration / 60:00}:{remainingDuration % 60:00}";
            UIFill.fillAmount = Mathf.InverseLerp(0,Duration, remainingDuration);
            remainingDuration--;
            yield return new WaitForSeconds(1f);
         }
         yield return null;
      }
      OpenPanel();
   }
   void OpenPanel()
   {
      UiText.text = "";
      panel.SetActive(true);
   }
}
