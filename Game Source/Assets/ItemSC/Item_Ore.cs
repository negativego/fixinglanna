using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Ore : MonoBehaviour ,IInteractable
{
    private Transform PickUpItem_;
    private GameObject InHandItem_;
    public bool Ishand_ = true;
    public int _CheckOre;
    public Smelter Smel;
    public PickUpChack _PickUpChack;
    
    public void Interact(GameObject actor)
    {
       
        if (Ishand_ == true)
        {
            // Debug.Log(Random.Range(1, 100));
            transform.localScale = new Vector3(0.2f, 0.2f, 0.35f);
            Rigidbody rb = new Rigidbody();
            InHandItem_ = gameObject;
            InHandItem_.transform.position = Vector3.forward;
            InHandItem_.transform.rotation = Quaternion.identity;
            //InHandItem_.transform.SetParent(PickUpItem_.transform, false);
            InHandItem_.transform.SetParent(GameObject.Find("Pickup").transform, false);
            if (rb != null)
            {
                rb.isKinematic = true;
            }

            _CheckOre = 1;
            Ishand_ = false;
           
          
           
          
          
        }


       

        else if (Ishand_ == false)
        {
            InHandItem_.transform.SetParent(null);
            InHandItem_ = null;
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
            }

            Ishand_ = true;
            _CheckOre = 2;
            
            //Debug.Log("Isfalse");
           
        }
        

        
    }public void DestroyItem()
    {
        
        Destroy(gameObject);
        Debug.Log("destroy");
        
    }
    private void Update()
    {
        Ishand_ = _PickUpChack._chck;
        if (InHandItem_ != null)
        {
            return;
        }
        transform.localScale = new Vector3(0.2f, 0.2f, 0.3f);
    }
    private void Start()
    {
        _CheckOre = 0;
       
        PickUpItem_ = GameObject.Find("Pickup").transform;
        _PickUpChack = GameObject.Find("Pickup").GetComponent<PickUpChack>();

    }
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.name == "Spawn_Smelter" )
        {
            Smel = GameObject.Find("Spawn_Smelter").GetComponent<Smelter>();  
            //Debug.Log("Pagoda");
        }
      
    }
    private void OnCollisionExit(Collision other)
    {
        Smel = null;
    }
    
}
