
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;
using TMPro;
using UnityEngine.UI;
[RequireComponent(typeof(Rigidbody))]

public class SpawnItem : MonoBehaviour, IInteractable
{
    [SerializeField] private  GameObject Prefab;
    
    [SerializeField] private Transform prefabTransform;
    [SerializeField] private Transform prefabTransform1;
    [SerializeField] private Transform prefabTransform2;
    [SerializeField] private int timedelay = 0;
    [SerializeField] private bool IsTimedelay;
    
    //
    [SerializeField] private float _SliderValue;
    [SerializeField] protected Slider m_SliderTimer;
    private bool _IsTimerStart = false;
    private float _StartTimeStamp;
    [SerializeField] protected TextMeshProUGUI m_InteractionTxt;



    public void Interact(GameObject actor)
    {
        if (IsTimedelay == false)
        {
            m_SliderTimer.gameObject.SetActive(true);
            StartTimer();
            _SliderValue += 20;
          
          
               // Instantiate(Prefab, prefabTransform.position, Quaternion.identity);
               
               // StartCoroutine(DelayTime());
        }

    }

    private  IEnumerator DelayTime()
    {
        IsTimedelay = true;
        yield return new WaitForSeconds(timedelay);
        IsTimedelay = false;
    }
    private void StartTimer()
    {
        //Check if the timer is already running
        if (_IsTimerStart) return;

        _IsTimerStart = true;
        _StartTimeStamp = Time.time;
        //_EndTimeStamp = Time.time + m_TimerDuration;
        _SliderValue = 0;
    }
    private void Update()
    {
        if (!_IsTimerStart) return;

       
        m_SliderTimer.value = _SliderValue;
        _SliderValue = _SliderValue;

        if ( _SliderValue == 100)
        {
            _IsTimerStart = false;
            int ranPos = Random.Range(1, 4);
            if (ranPos == 1)
            {
                Instantiate(Prefab, prefabTransform.position, Quaternion.identity);
            }
             if (ranPos == 2)
            {
                Instantiate(Prefab, prefabTransform1.position, Quaternion.identity);
            }

             if (ranPos == 3)
             {
                 Instantiate(Prefab, prefabTransform2.position, Quaternion.identity);
             }
//            Debug.Log(ranPos);
            m_SliderTimer.gameObject.SetActive(false);
                
        }

            
    }
    
    
    
}
