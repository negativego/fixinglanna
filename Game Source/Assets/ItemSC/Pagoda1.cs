using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class Pagoda1 : MonoBehaviour ,IInteractable
{
    private GameObject _step00;
    private GameObject _step01;
    private GameObject _step02;
    private GameObject _step03;
    private GameObject _step04;
    private int _stpeNumber = 1;
    private int _CHG = 0;
    [SerializeField] Item_Gold _itemGold;
    public void Interact(GameObject actor)
    {


       

        if (_CHG == 1)
        {
            switch (_stpeNumber)
            {
                
                case 1:_itemGold.DestroyItem();
                    _step03.gameObject.SetActive(true);
                    _stpeNumber = 2;
                   
                    break;
                
                case 2 :_itemGold.DestroyItem();
                    _step04.gameObject.SetActive(true);
                   
                    break;
            }
        }
        
    }
    
    void Start()
    {
        _step01 = GameObject.Find("S_1");
        _step02 = GameObject.Find("S_2");
        _step03 = GameObject.Find("S_3");
        _step04 = GameObject.Find("S_4");
        
        //Set Wall
        _step01.gameObject.SetActive(true);
        _step02.gameObject.SetActive(true);
        _step03.gameObject.SetActive(false);
        _step04.gameObject.SetActive(false);
       



    }
    
    private void OnCollisionStay(Collision other)
    {
        
       //other.gameObject.TryGetComponent<Item_Dirt>(out Item_Dirt I_D);
        other.gameObject.TryGetComponent<Item_Gold>(out Item_Gold I_G);
       //_itemDirt = I_D;
        _itemGold = I_G;
        //_CHD = _itemDirt._chackItemDirt;
        _CHG = _itemGold._ChackItemGold;
    }
 
    private void OnCollisionExit(Collision other)
    {
       
       
       //_CHD = 0;
       _CHG = 0;
    }
    void Update()
    {

       //_CHD = _itemDirt._chackItemDirt;
       _CHG = _itemGold._ChackItemGold;
       
        
        

    }

}
