using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using  UnityEngine.UI;

public class Interact_Wall : MonoBehaviour ,IInteractable
{
    [SerializeField] public GameObject Tutorial;
    [SerializeField] public GameObject Winning;
    [SerializeField] public GameObject panelPhase;
    [SerializeField] public TMP_Text Phase;
    private int PhaseInt = 2;
    private int Itemint = 0;
    private bool Check;
    
    
    private GameObject _step01;
    private GameObject _step02;
    private GameObject _step03;
    private GameObject _step04;
    private int _stpeNumber = 1;
    private int _itemNum = 0;
    [SerializeField] Iten_Process iTP;
   public int _CIH;
   private int Din =0;
    public void Interact(GameObject actor)
    {
        Debug.Log("Wall_Interact");
       if (_CIH == 1)
        {
            switch (_stpeNumber)
            {
                case 1 :
                    iTP.DestroyItem();
                    _itemNum += 1;
                    
                    if (_itemNum == 2)
                    {
                        _step01.gameObject.SetActive(true);
                       
                        PhaseInt += 1;
                        _itemNum = 0;
                        _stpeNumber = 2;
                    }
                    break;
                case 2 : iTP.DestroyItem();
                    _itemNum += 1;
                    if (_itemNum == 3)
                    {
                        _step01.gameObject.SetActive(false);
                   
                        _step02.gameObject.SetActive(true);
                       
                        PhaseInt += 1;
                        _stpeNumber = 3;
                        _itemNum = 0;
                    }                        
                    
                   
                    break;
                case 3 :iTP.DestroyItem();
                    _itemNum += 1;
                    if (_itemNum == 4)
                    {
                        _step02.gameObject.SetActive(false);
                    
                        _step03.gameObject.SetActive(true);
                   
                        PhaseInt += 1;
                        _stpeNumber = 4;
                        _itemNum = 0;
                    }
                    
                    
                    break;
               
                
                
                case 4:iTP.DestroyItem();
                    _itemNum += 1;
                    
                    if (_itemNum == 5)
                    {
                        Unlock();
                        _step04.gameObject.SetActive(true); 
                        
                        _stpeNumber = 5;
                    }
                   
                    break;
                
                
            }
        }
        
        
        

        
            
        
       
    }
    void Start()
    {
        Tutorial.SetActive(true);
        Winning.SetActive(false);
        GetComponent<FinishPoint>();
        
        _step01 = GameObject.Find("Phase_1");
        _step02 = GameObject.Find("Phase_2");
        _step03 = GameObject.Find("Phase_3");
        _step04 = GameObject.Find("Phase_4");
        
        //Set Wall
        _step01.gameObject.SetActive(false);
        _step02.gameObject.SetActive(false);
        _step03.gameObject.SetActive(false);
        _step04.gameObject.SetActive(false);


       // iTP = GameObject.Find("Item_Process_Ob").GetComponent<Iten_Process>();
        _CIH = 0;
    }

  
    private void OnCollisionStay(Collision other)
    {
        other.gameObject.TryGetComponent<Iten_Process>(out Iten_Process I_P);
        iTP = I_P;
       _CIH = iTP._chackItemWall;
    }
 
    private void OnCollisionExit(Collision other)
    {
        //iTP = null;
        _CIH = 0;
    }
    
    
  
    void Update()
    {
        Phase.text = "Brick "+(_itemNum)+"/"+(PhaseInt)+" to "+"Fix";
        
        _CIH = iTP._chackItemWall;
     

    }
    void Unlock()
    {
        Debug.Log(("Unlock"));
       // gameObject.GetComponent<FinishPoint>().UnlockNewLevel();
        GameObject.Find("CheckFinish").GetComponent<FinishPoint>().UnlockNewLevel();
        Winning.SetActive(true);
        Check = true;
    }
    IEnumerator Delay()
    {
        
        yield return new WaitForSeconds(1);
        Debug.Log(("CheckUnlock"));
        Unlock();
    }
}
