using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Processing : MonoBehaviour ,IInteractable 
{
    [SerializeField] protected TextMeshProUGUI m_TextInfoEToInteract;
    [SerializeField] protected float m_TimerDuration = 5;

    [SerializeField] protected Slider m_SliderTimer;
    private bool _IsTimerStart = false;
    private float _StartTimeStamp;
    private float _EndTimeStamp;
    private float _SliderValue;
   
    /// /////////
    
    [SerializeField] private GameObject Prefab;
    [SerializeField] private Transform prefabTransform;
    [SerializeField]  Item_Assets iTA;
   
    private int _IH;
    
   // private int IsTimedelay;
   
    
    public void Interact(GameObject actor)
    {

        
            if (_IH ==1)
            {
              
           
                Debug.Log("Process");
                iTA.DestroyItem();
                
                StartTimer();
                DelayTime();
                




            }
        
          
        
        
            
        
       
    }
    
    private void StartTimer()
    {

        if (_IsTimerStart) return;

        _IsTimerStart = true;
        _StartTimeStamp = Time.time;
        _EndTimeStamp = Time.time + m_TimerDuration;
        _SliderValue = 0;
        m_SliderTimer.gameObject.SetActive(true);
        
    }
    private void Update()
    {
        
        
        if (!_IsTimerStart) return;

        _SliderValue = ((Time.time - _StartTimeStamp)/m_TimerDuration)*m_SliderTimer.maxValue;
        m_SliderTimer.value = _SliderValue;

        if (Time.time >=_EndTimeStamp)
        {
            _IsTimerStart = false;
            m_SliderTimer.gameObject.SetActive(false);
            Instantiate(Prefab, prefabTransform.position, Quaternion.identity);
        }
        
        
    }

    private void Start()
    {
        
       // IsTimedelay = 0;
    }

    private void OnCollisionStay(Collision other)
    {
        other.gameObject.TryGetComponent<Item_Assets>(out Item_Assets I_A);
        iTA = I_A;
        _IH = iTA._CheckIten;
    }

    private void OnCollisionExit(Collision other)
    {
        iTA = null;
        _IH = 0;
    }
    
    
    
    private  IEnumerator DelayTime()
    {
        _IH = 0;
        yield return new WaitForSeconds(10);
        _IH = iTA._CheckIten;


    }
}
