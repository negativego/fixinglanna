using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Iten_Process : MonoBehaviour,IInteractable
{
    [SerializeField] public Transform PickUpItem_;
    [SerializeField] private GameObject InHandItem_;
    public  bool Ishand_ = true;
    private Interact_Wall _itenProcess;
   public int _chackItemWall;
   public PickUpChack _PickUpChack;
   
    
    public void Interact(GameObject actor)
    {
       
        if (Ishand_ == true)
        {
            
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            Debug.Log("11111");
            Rigidbody rb = new Rigidbody();
            InHandItem_ = gameObject;
            InHandItem_.transform.position = Vector3.forward;
            InHandItem_.transform.rotation = Quaternion.identity;
            //InHandItem_.transform.SetParent(PickUpItem_.transform, false);
            InHandItem_.transform.SetParent(GameObject.Find("Pickup").transform, false);
            if (rb != null)
            {
                rb.isKinematic = true;
            }

            Ishand_ = false;
            _chackItemWall = 1;

        }

      

        else if (Ishand_ == false)
        {
            Debug.Log("2222222");
            InHandItem_.transform.SetParent(null);
            InHandItem_ = null;
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
            }

            Ishand_ = true;
            _chackItemWall = 2;
          
           
        }
        
        
    }
    public void Update()
    {
      
        Ishand_ = _PickUpChack._chck;
        //Debug.Log(ProDes);
        if (InHandItem_ != null)
        {
            return;
        }
        transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        
    }
   public void Start()
    {
      
        PickUpItem_ = GameObject.Find("Pickup").transform;
        _chackItemWall = 0;
        _PickUpChack = GameObject.Find("Pickup").GetComponent<PickUpChack>();

    }
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.name == "wall" )
        {
            _itenProcess = GameObject.Find("wall").GetComponent<Interact_Wall>();     
        }
      
    }

    private void OnCollisionExit(Collision other)
    {
        _itenProcess = null;
    }
    
    public void DestroyItem()
    {
        
        Destroy(gameObject);
        Debug.Log("destroy");
        
    }
}
