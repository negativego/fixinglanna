using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class Item_Assets : MonoBehaviour, IInteractable
{
    public  Processing Pro;
     public Transform PickUpItem_;
    private GameObject InHandItem_;
    public bool Ishand_;
    public int _CheckIten ;
    
    /// 
    public PickUpChack _PickUpChack;
    
    
  


    public void Interact(GameObject actor)
    {
       
        if (Ishand_ == true)
        {
            
           
           // Debug.Log(Random.Range(1, 100));
           transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            Rigidbody rb = new Rigidbody();
            InHandItem_ = gameObject;
            InHandItem_.transform.position = Vector3.forward;
            InHandItem_.transform.rotation = Quaternion.identity;
            
            //InHandItem_.transform.SetParent(PickUpItem_.transform, false);
            InHandItem_.transform.SetParent(GameObject.Find("Pickup").transform, false);
            if (rb != null)
            {
                rb.isKinematic = true;
              
               
            }

           
           
            _CheckIten = 1;
            Ishand_ = false;
           
          
          
        }
         else if (Ishand_ == false )
        {
            
            InHandItem_.transform.SetParent(null);
            InHandItem_ = null;
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
                rb.useGravity = false;
            }

            
            _CheckIten = 2;
            Debug.Log("Isfalse");
            Ishand_ = false;
           
        }
    }

    public void DestroyItem()
    {
        
        Destroy(gameObject);
        Debug.Log("destroy");
        
    }

   private void Start()
   {
       
       _CheckIten = 0;
      
       
       PickUpItem_ = GameObject.Find("Pickup").transform;
       _PickUpChack = GameObject.Find("Pickup").GetComponent<PickUpChack>();


   }
    private void Update()
    {
        if (InHandItem_ != null)
        {
            return;
        }
        transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);


        Ishand_ = _PickUpChack._chck;

        //Debug.Log(_PickUpChack._chck);


    }

  
   //In put SC
   private void OnCollisionStay(Collision other)
   {
       if (other.gameObject.name == "Spawn_process" )
       {
           Pro = GameObject.Find("Spawn_process").GetComponent<Processing>();     
       }
   }

   private void OnCollisionExit(Collision other)
   {
       Pro = null;
   }

  
}
