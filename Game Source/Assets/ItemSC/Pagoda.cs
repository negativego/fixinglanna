using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Pagoda : MonoBehaviour ,IInteractable
{
    [SerializeField] public GameObject Tutorial;
    [SerializeField] public GameObject Winning;
    [SerializeField] public GameObject panelPhase;
    [SerializeField] public TMP_Text Phase;
    public int PhaseIntD = 3;
    public int PhaseIntG = 4;
    public int Phase2IntG = 5;
    private bool Check;
    private int TextNumb;
    public int PhaseInt = 0;
    private string TextMarterial;
    
    private GameObject _step00;
    private GameObject _step01;
    private GameObject _step02;
    private GameObject _step03;
    private GameObject _step04;
    private int _stpeNumber = 1;
    private int _CHD = 0;
    private int _CHG = 0;
    [SerializeField] Item_Dirt _itemDirt;
    [SerializeField] Item_Gold _itemGold;
    private int _NumD = 0;
    private int _NumG = 0;
 
    public void Interact(GameObject actor)
    {


        if (_CHD == 1)
        {
            switch (_stpeNumber)
            {
                  
                case 1 :_itemDirt.DestroyItem();
                    _NumD += 1;
                    TextNumb = _NumD;
                    if (_NumD == 3)
                    {
                        _NumD = 0;
                        _itemDirt.DestroyItem();
                        _step01.gameObject.SetActive(true);
                        _stpeNumber = 2;
                        
                    }
                    
                   
                    break;
                    
                case 2 :_itemDirt.DestroyItem();
                    _NumD += 1;
                    TextNumb = _NumD;
                    if (_NumD == 3 )
                    {
                        TextMarterial = "Gold ";
                        _step01.gameObject.SetActive(false);
                        _step02.gameObject.SetActive(true);
                        _stpeNumber = 3;
                        _NumD = 0;
                        TextNumb = _NumG;
                        PhaseInt = PhaseIntG;
                    }
                   
                    break;
                
                
            }
        }

        if (_CHG == 1)
        {
            switch (_stpeNumber)
            {
                
                case 3:_itemGold.DestroyItem();
                    _NumG += 1;
                    TextNumb = _NumG;
                    if (_NumG == 4)
                    {
                        _NumG = 0;
                        TextNumb = _NumG;
                        PhaseInt += 1;
                        _step02.gameObject.SetActive(false);
                        _step03.gameObject.SetActive(true);
                        _stpeNumber = 4;
                    }
                    
                    break;
                
                case 4 :_itemGold.DestroyItem();
                    _NumG += 1;
                    TextNumb = _NumG;
                    if (_NumG == 5)
                    {
                        Winning.SetActive(true);
                        Unlock();
                        _step03.gameObject.SetActive(false);
                        _step04.gameObject.SetActive(true);
                        _stpeNumber = 5;
                    }
                    break;
            }
        }
        
    }
    
    void Start()
    {
        Time.timeScale = 0f;
        TextMarterial = "Dirt ";
        PhaseInt = PhaseIntD;
        
        Tutorial.SetActive(true);
        Winning.SetActive(false);
        GetComponent<FinishPoint>();

        
        _step01 = GameObject.Find("S_1");
        _step02 = GameObject.Find("S_2");
        _step03 = GameObject.Find("S_3");
        _step04 = GameObject.Find("S_4");
        
        //Set Wall
        _step01.gameObject.SetActive(false);
        _step02.gameObject.SetActive(false);
        _step03.gameObject.SetActive(false);
        _step04.gameObject.SetActive(false);
       



    }
    
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.TryGetComponent<Item_Dirt>(out Item_Dirt I_D))
        {
            _itemDirt = I_D;
       
            _CHD = _itemDirt._chackItemDirt;
        }
       
       if (other.gameObject.TryGetComponent<Item_Gold>(out Item_Gold I_G))
       {
           _itemGold = I_G;
           _CHG = _itemGold._ChackItemGold;
       }
       
      
    }
 
    private void OnCollisionExit(Collision other)
    {
       
       
       _CHD = 0;
       _CHG = 0;
    }
    void Update()
    {
        Phase.text = (TextMarterial)+(TextNumb)+"/"+(PhaseInt)+" to "+"Fix";
        
        if (_itemDirt)
        {
            _CHD = _itemDirt._chackItemDirt;
        }

        if (_itemGold)
        {
            _CHG = _itemGold._ChackItemGold;

        }
    }
    
    void Unlock()
    {
        Debug.Log(("Unlock"));
        GameObject.Find("CheckFinish").GetComponent<FinishPoint>().UnlockNewLevel();
        Winning.SetActive(true);
        Check = true;
    }


}
