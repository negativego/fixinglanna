using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
public class Smelter : MonoBehaviour ,IInteractable

{   //Time Proces
    [SerializeField] protected TextMeshProUGUI m_TextInfoEToInteract;
    [SerializeField] protected float m_TimerDuration = 5;

    [SerializeField] protected Slider m_SliderTimer;
    private bool _IsTimerStart = false;
    private float _StartTimeStamp;
    private float _EndTimeStamp;
    private float _SliderValue;
   //////////////////////
   [SerializeField] private GameObject Prefab;
   [SerializeField] private Transform prefabTransform;
   private Item_Ore _itemOre;
   private int _IH;
   
   
 
   
   public void Interact(GameObject actor)
   {

        
       if (_IH ==1)
       {    // Debug.Log("Process");
          _itemOre.DestroyItem();
                
           StartTimer();
           
                
       }
        
       
   }
   private void StartTimer()
   {

       if (_IsTimerStart) return;

       _IsTimerStart = true;
       _StartTimeStamp = Time.time;
       _EndTimeStamp = Time.time + m_TimerDuration;
       _SliderValue = 0;
       m_SliderTimer.gameObject.SetActive(true);
        
   }
   private void Update()
   {
        
        
       if (!_IsTimerStart) return;

       _SliderValue = ((Time.time - _StartTimeStamp)/m_TimerDuration)*m_SliderTimer.maxValue;
       m_SliderTimer.value = _SliderValue;

       if (Time.time >=_EndTimeStamp)
       {
           _IsTimerStart = false;
           m_SliderTimer.gameObject.SetActive(false);
           Instantiate(Prefab, prefabTransform.position, Quaternion.identity);
       }
        
        
   }
   private void OnCollisionStay(Collision other)
   {
       other.gameObject.TryGetComponent<Item_Ore>(out Item_Ore I_O);
       _itemOre = I_O;
       _IH = _itemOre._CheckOre;
   }

   private void OnCollisionExit(Collision other)
   {
       _itemOre = null;
       _IH = 0;
   }


   private void Start()
   {
        
      
   }
}
