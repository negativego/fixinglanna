using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpChack : MonoBehaviour
{
    
    public bool _chck;
    private Animator animator;

    
    
    // Start is called before the first frame update
    void Start()
    {
        _chck = true;
        animator = this.GetComponent<Animator>();
    
    }

    // Update is called once per frame
    void Update()
    {
       
        GameObject parentObject = GameObject.Find("Pickup");
        

       
        if (parentObject.transform.childCount <= 0)
        {
        
            _chck =true;
            animator.SetBool("Pick",false);
            
            
            
        }
        else
        {
            _chck = false;
            animator.SetBool("Pick",true);
            
            

        }
    }

}