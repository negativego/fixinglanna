using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Gold : MonoBehaviour,IInteractable
{
    private Transform PickUpItem_;
    private GameObject InHandItem_;
    public bool Ishand_ = true;
    public int _ChackItemGold;
    public PickUpChack _PickUpChack;
    public void Interact(GameObject actor)
    {
       
        if (Ishand_ == true)
        {
            // Debug.Log(Random.Range(1, 100));
            transform.localScale = new Vector3(0.2f, 0.2f, 0.35f);
            Rigidbody rb = new Rigidbody();
            InHandItem_ = gameObject;
            InHandItem_.transform.position = Vector3.forward;
            InHandItem_.transform.rotation = Quaternion.identity;
            //InHandItem_.transform.SetParent(PickUpItem_.transform, false);
            InHandItem_.transform.SetParent(GameObject.Find("Pickup").transform, false);
            if (rb != null)
            {
                rb.isKinematic = true;
            }

            Ishand_ = false;

            _ChackItemGold = 1;



        }


       

        else if (Ishand_ == false)
        {
            InHandItem_.transform.SetParent(null);
            InHandItem_ = null;
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
            }

            Ishand_ = true;
            
            Debug.Log("Isfalse");
            _ChackItemGold = 2;
           
        }
        

        
    } public void DestroyItem()
    {
        
        Destroy(gameObject);
        Debug.Log("destroy");
        
    } private void Update()
    {
        Ishand_ = _PickUpChack._chck;
        if (InHandItem_ != null)
        {
            return;
        }
        transform.localScale = new Vector3(0.2f, 0.2f, 0.3f);
    }
    private void Start()
    {
        
       
        PickUpItem_ = GameObject.Find("Pickup").transform;
        _PickUpChack = GameObject.Find("Pickup").GetComponent<PickUpChack>();

    }
    
}
