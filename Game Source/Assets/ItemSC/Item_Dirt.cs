using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Dirt : MonoBehaviour ,IInteractable
{
    private Transform PickUpItem_;
    private GameObject InHandItem_;
    public bool Ishand_ = true;
    private Pagoda Pago;
    public int _chackItemDirt;
    public PickUpChack _PickUpChack;
    public void Interact(GameObject actor)
    {
       
        if (Ishand_ == true)
        {
           
            // Debug.Log(Random.Range(1, 100));
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
           //test
           //
            InHandItem_ = gameObject;
           
            InHandItem_.transform.position = Vector3.forward;
            InHandItem_.transform.rotation = Quaternion.identity;
            //InHandItem_.transform.SetParent(PickUpItem_.transform, false);
            InHandItem_.transform.SetParent(GameObject.Find("Pickup").transform, false);
            Rigidbody rb = new Rigidbody();
            if (rb != null)
            {
                rb.isKinematic = true;
                
            }
            

            Ishand_ = false;    


            _chackItemDirt = 1;
            

        }
      else if (Ishand_ == false)
        {
            InHandItem_.transform.SetParent(null);
            InHandItem_ = null;
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
            }

            Ishand_ = true;
            
            Debug.Log("Isfalse");
            _chackItemDirt = 2;
        }
        

        
    }
    public void DestroyItem()
    {
        
        Destroy(gameObject);
        Debug.Log("destroy");
        
    }
    public void Update()
    {
        Ishand_ = _PickUpChack._chck;
        if (InHandItem_ != null)
        {
            return;
        }
        transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        
    }
    private void Start()
    {
        
       
        PickUpItem_ = GameObject.Find("Pickup").transform;
        _PickUpChack = GameObject.Find("Pickup").GetComponent<PickUpChack>();

    }
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.name == "Pagoda" )
        {
            Pago = GameObject.Find("Pagoda").GetComponent<Pagoda>();  
            //Debug.Log("Pagoda");
        }
      
    }
    private void OnCollisionExit(Collision other)
    {
        Pago = null;
    }
   
    
  
   
}
