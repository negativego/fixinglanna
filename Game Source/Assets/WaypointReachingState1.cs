﻿using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

namespace Samarnggoon.GameDev3.Chapter12.SimpleWaypointAIFSM
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonControllerAI1))]
    public class WaypointReachingState1 : MonoBehaviour
    
    {
        public Transform player;
        public float chaseDistance = 10f;
        public float delayHit = 5;
        ///
       
        
        
        [SerializeField] protected int m_CurrentWaypointIndex=0;

        [SerializeField] protected ThirdPersonControllerAI1 m_3rdPersonControllerAI;
       
        [SerializeField] protected NavMeshAgent m_NavMeshAgent;

        [SerializeField] protected List<Transform> m_Waypoints;

        private void Start()
        {
            m_NavMeshAgent = GetComponent<NavMeshAgent>();
            m_3rdPersonControllerAI = GetComponent<ThirdPersonControllerAI1>();
          
        }

        public void EnterState()
        {
            m_NavMeshAgent.SetDestination(m_Waypoints[m_CurrentWaypointIndex].position);
        }
        
        public void UpdateState()
        {
            if (m_NavMeshAgent.remainingDistance > m_NavMeshAgent.stoppingDistance)
            {
                m_3rdPersonControllerAI.Move(m_NavMeshAgent.desiredVelocity);
            }
            else
            {
                m_3rdPersonControllerAI.Move(Vector3.zero);

                if (m_CurrentWaypointIndex + 1 < m_Waypoints.Count)
                {
                    m_CurrentWaypointIndex++;
                    CustomEvent.Trigger(gameObject,"GotoIdleState");
                }
                else
                {//reach the maximum of the available waypoints, then go back to 0
                    m_CurrentWaypointIndex = 0;
                    CustomEvent.Trigger(gameObject,"GotoIdleState");
                }
            }
        }

        public void ExitState()
        {
            
        }

        public void Update()
        {
            float distanceToPlayer = Vector3.Distance(transform.position, player.position);
            if (distanceToPlayer <= chaseDistance)
            {
              // Debug.Log("Ch");
               m_NavMeshAgent.SetDestination(player.position);
            
            }else if (distanceToPlayer >= chaseDistance)
            {
                EnterState();
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                EnterState();
                Invoke("Update",delayHit);
         
            }
            if (collision.gameObject.CompareTag("K9"))
            {
                EnterState();
                Debug.Log("K9");
         
            }
        }
    }
}