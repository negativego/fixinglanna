using UnityEngine;
using UnityEngine.UI;

namespace GameDev3.Chapter11
{
public class SoundManager : MonoBehaviour
{
[SerializeField] protected SoundSettings m_SoundSettings;

public Slider m_SliderMasterVolume;
public Slider m_SliderMusicVolume;
public Slider m_SliderMasterSFXVolume;

// Start is called before the first frame update
void Start()
{
InitialiseVolumes();
}

private void InitialiseVolumes()
{
SetMasterVolume(m_SoundSettings.MasterVolume);
SetMusicVolume(m_SoundSettings.MusicVolume);
SetMasterSFXVolume(m_SoundSettings.MasterSFXVolume);
}

public void SetMasterVolume(float vol)
{
//Set float to the audiomixer
m_SoundSettings.AudioMixer.SetFloat(m_SoundSettings.MasterVolumeName,vol);

m_SoundSettings.MasterVolume = vol;
//Set the slider bar’s value
m_SliderMasterVolume.value = m_SoundSettings.MasterVolume;
}
public void SetMusicVolume(float vol)
{
m_SoundSettings.AudioMixer.SetFloat(m_SoundSettings.BGVolume,vol);
m_SoundSettings.MusicVolume = vol;
//Set the slider bar’s value
m_SliderMusicVolume.value = m_SoundSettings.MusicVolume;
}
public void SetMasterSFXVolume(float vol)
{
//Set float to the audiomixer
m_SoundSettings.AudioMixer.SetFloat(m_SoundSettings.SFXVolume,vol);

m_SoundSettings.MasterSFXVolume = vol;
//Set the slider bar’s value
m_SliderMasterSFXVolume.value = m_SoundSettings.MasterSFXVolume;
}
}
}
