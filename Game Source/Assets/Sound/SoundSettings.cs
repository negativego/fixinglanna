using UnityEngine;
using UnityEngine.Audio;

namespace GameDev3.Chapter11
{
    [CreateAssetMenu(menuName = "GameDev3/Chapter11/SoundSettingsPreset",
    fileName = "SoundSettingsPreset")]
    public class SoundSettings : ScriptableObject
    {
        public AudioMixer AudioMixer;

        [Header("Master")]
        public string MasterVolumeName = "MasterVolume";
        [Range(-80,20)]
        public float MasterVolume;

        [Header("BG")]
        public string BGVolume ="BGVolume";
        [Range(-80,20)]
        public float MusicVolume;

        [Header("SFXVolume")]
        public string SFXVolume = "SFXVolume";
        [Range(-80,20)]
        public float MasterSFXVolume;

    }
}