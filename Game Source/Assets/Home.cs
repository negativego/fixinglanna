using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;

public class Home : MonoBehaviour,IInteractable
{
    [SerializeField] public GameObject Tutorial;
    [SerializeField] public GameObject Winning;
    [SerializeField] public GameObject panelPhase;
    [SerializeField] public TMP_Text Phase;
    private int PhaseInt = 2;
    private int Itemint = 0;
    private bool Check;
    
    
    
    private GameObject _step00;
    private GameObject _step01;
    private GameObject _step02;
    private GameObject _step03;
    private GameObject _step04;
    private int _stpeNumber = 1;
    private int _itemNum = 0;
    [SerializeField] Item_Log iTL;
    public int _CIH;
    private int Din =0;
  

    public void Interact(GameObject actor)
    {
       
        if (_CIH == 1)
        {
            switch (_stpeNumber)
            {
                case 1:
                    iTL.DestroyItem();
                    _itemNum += 1;

                    if (_itemNum == 2)
                    {
                        _step01.gameObject.SetActive(true);

                       PhaseInt += 2;
                        _itemNum = 0;
                        _stpeNumber = 2;
                        _step00.gameObject.SetActive(false);
                        
                    }
                    
                    

                    break;
                case 2:
                    iTL.DestroyItem();
                    _itemNum += 1;
                    if (_itemNum == 4)
                    {
                        _step01.gameObject.SetActive(false);

                        _step02.gameObject.SetActive(true);

                        PhaseInt = 4;
                        _stpeNumber = 3;
                        _itemNum = 0;
                    }


                    break;
                case 3:
                    iTL.DestroyItem();
                    _itemNum += 1;
                    if (_itemNum == 4)
                    {
                        _step02.gameObject.SetActive(false);

                        _step03.gameObject.SetActive(true);

                        PhaseInt = 2;
                        _stpeNumber = 4;
                        _itemNum = 0;
                    }
                    break;
                case 4:
                    iTL.DestroyItem();
                    _itemNum += 1;

                    if (_itemNum == 2)
                    {
                        Winning.SetActive(true);
                        Unlock();
                        _step04.gameObject.SetActive(true);
                        _stpeNumber = 5;
                    }
                    break;
            }
        }
    }

    void Start()
    {
        Tutorial.SetActive(true);
        Winning.SetActive(false);
        GetComponent<FinishPoint>();
        
        _step00 = GameObject.Find("BGH");
        _step01 = GameObject.Find("S_1");
        _step02 = GameObject.Find("S_2");
        _step03 = GameObject.Find("S_3");
        _step04 = GameObject.Find("S_4");
        
        //Set Wall
        _step00.gameObject.SetActive(true);
        _step01.gameObject.SetActive(false);
        _step02.gameObject.SetActive(false);
        _step03.gameObject.SetActive(false);
        _step04.gameObject.SetActive(false);


       
        _CIH = 0;
    }
    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.TryGetComponent<Item_Log>(out Item_Log I_P))
        {
               
            iTL = I_P;
            _CIH = iTL._ChackItemLog;

        }
         
        
         
       
    }
 
    private void OnCollisionExit(Collision other)
    {
        iTL = null;
        _CIH = 0;
    }
  
    
    void Update()
    {
       Phase.text = "Plank "+(_itemNum)+"/"+(PhaseInt)+" to "+"Fix";
        
        _CIH = iTL._ChackItemLog;
        Debug.Log(_CIH);
      
    }
    void Unlock()
    {
        Debug.Log(("Unlock"));
        //gameObject.GetComponent<FinishPoint>().UnlockNewLevel();
        GameObject.Find("CheckFinish").GetComponent<FinishPoint>().UnlockNewLevel();
        Winning.SetActive(true);
        Check = true;
    }
    IEnumerator Delay()
    {
        
        yield return new WaitForSeconds(1);
        Debug.Log(("CheckUnlock"));
        Unlock();
    }
}
